//  Created by Tymoteusz Stokarski on 18/11/2020.
//

import Foundation

enum LinkURL: String {
    
    case labourLaw = "http://isap.sejm.gov.pl/isap.nsf/download.xsp/WDU19740240141/U/D19740141Lj.pdf"
    case leaveBenefits = "http://isap.sejm.gov.pl/isap.nsf/download.xsp/WDU19990600636/U/D19990636Lj.pdf"
    
}
